﻿
using System.Net.Mail; 

namespace XML2Mail
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Xml.Linq;
    using System.Xml;               // XDocument throws XMLReader's exceptions.
    using System.Net; 
    using System.Security;

    using SDS.Scheduler;


    namespace XML2Mail
    {
        /**
         * Responsible for reading an XML format and mapping this to the model structure.
         * I want to keep things as simple as possible.
         */
        public class JobIO
        {
            public ResultCode readFile(string fullFilePath, ref XDocument xdoc, ref string executionInfo)
            {
                string badArguments = "";

                if (String.IsNullOrEmpty(fullFilePath)) badArguments += "fullFilePath:null or empty";
                if (!badArguments.Equals("")) throw new ArgumentException(badArguments);

                if (!File.Exists(fullFilePath))
                    return ResultCode.rcResourceDoesNotExist;

                try
                {
                    xdoc = XDocument.Load(fullFilePath); // All we do...

                } catch(XmlException xmle)
                {
                    executionInfo += "The file: " + fullFilePath + " could not be loaded as an XML document. Technical details: " + xmle.Message; 
                    return ResultCode.rcInvalidInput; 
                } catch(Exception e)
                {
                    executionInfo += "Loading the file " + fullFilePath + " as an XML document gave an unexpected exception: " + e.Message; 
                    return ResultCode.rcError; 
                }
                return ResultCode.rcOk; 
            }
            /**
             * Checks a directory for an input file. If any valid input file format is found, one is opened, loaded and considered for
             * processing. 
             * @param inputDirectory The full directory path (including drive name, etc.) 
             * @param faultyInputDirectory The full directory path where faulty input is placed, along with a as human 
             * friendly as possible explanation *cough*.
             * @return 
             *  ResultCode.rcOk if a file was succesfully opened as XDocument. 
             *  ResultCode.rcNoInput if no file was found for processing, in which case the caller should ignore the value of loadedXDocument. 
             * @throws 
             */
            public ResultCode readInput(IJobContext jobContext, DirectoryInfo diInput, DirectoryInfo diWork, DirectoryInfo diFaultyInput, ref XDocument loadedXDocument, ref FileInfo foundFileInfo, ref string executionInfo)
            {
                ResultCode rc = ResultCode.rcError;
                // Scan directory contents for any XML file. 

                FileInfo[] fileInfos = diInput.GetFiles("*.xml");

                // Do we have at least one?
                if (fileInfos.Count() == 0)
                {
                    jobContext.LogMessage(XML2MailJobImpl.MSG_INFO, "No input available.");
                    return ResultCode.rcNoInput;
                }

                try
                {
                    FileInfo firstFile = fileInfos[0]; // Set the ref sooner, so caller can still move the file after an XML exception occurred.
                    rc = moveJobFileTo(null, firstFile, diWork, false);
                    if (rc != ResultCode.rcOk)
                    {
                        jobContext.LogMessage(XML2MailJobImpl.MSG_ERROR, "Could not move file: " + firstFile.FullName + " to working directory. Code: " + rc);
                        return rc;
                    }
                    foundFileInfo = firstFile; // Assign before loading

                    loadedXDocument = XDocument.Load(firstFile.FullName);
                    jobContext.LogMessage(XML2MailJobImpl.MSG_INFO, "Found a file to handle: " + firstFile.FullName);

                }
                catch (Exception e) // XMLException - For some reason this did not catch xml exceptions thrown by 
                {
                    string msg = "The file found (" + fileInfos[0].FullName + ") contained invalid XML formatting.";
                    jobContext.LogMessage(XML2MailJobImpl.MSG_ERROR, msg);
                    // loadedXDocument not a disposable or even a closable, assumption is that Load() cleans up and closes properly in case of an exception.
                    executionInfo += "Invalid format? " + e.Message;
                    return ResultCode.rcInvalidInput;
                }

                return ResultCode.rcOk;
            }
            /**
             * Compound function. Given the job, we can only cleanup as we are given a chance to run. 
             * Whenever a file is moved, a trim must occur forehand on the directory the file moves to.
             * 
             * Places faulty input to the faulty input directory. 
             * Creates a directory to write the faulty input in, along with a small readme message with an indication of the fault.
             * Additionally checks whether there are any directories older than 4 days and removes those (cleanup action), unless
             * the directory contains more than 10 megabytes of faulty input, in which case it will trim away the oldest until +/- 8
             * megabytes remain.
             */
            public ResultCode moveJobFileTo(string message, FileInfo file, DirectoryInfo diTargetDir, bool createOwnDir = true, string msgFileName = "message.txt")
            {
                DirectoryInfo writeDir = null;

                // A timestamp.
                try
                {
                    if (createOwnDir)
                    {
                        writeDir = diTargetDir.CreateSubdirectory("HBJ_" + DateTime.Now.ToString("yy_dd_M_HH_mm_ss"));
                    }
                    else
                    {
                        writeDir = diTargetDir;
                    }

                    file.MoveTo(writeDir.FullName + Path.DirectorySeparatorChar + file.Name);


                    // Do we have a message? If so write it into its own file.
                    if (message != null && message.Length > 0)
                    {
                        // Added time to create unique. 
                        FileInfo errorLog = new FileInfo(writeDir.FullName + Path.DirectorySeparatorChar + "_" + msgFileName);

                        FileStream fs = errorLog.Create();

                        Byte[] msgData = new UTF8Encoding(true).GetBytes(message);
                        fs.Write(msgData, 0, msgData.Length);

                        fs.Close();
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("Exception while moving file: " + e.Message);
                    return ResultCode.rcError;
                }
                return ResultCode.rcOk;
            }
            /**
             * Trims content in a directory, based on age and max allowed quantity (in bytes). First cleans
             * up all files (and directories) based on age. If the remaining size exceeds the quantity allowed,
             * entries are deleted until the content size is allowed. 
             * 
             * Expects parameters to be checked by the caller.
             */
            public ResultCode trimDir(DirectoryInfo directoryToTrim, DateTime tooOld, int maxQuantityInKB, ref int countTrimByAge, ref int countTrimBySize)
            {
                long totalSize = 0;
                bool directoryDeleted = false; // Just a stack var to pass by reference, function does not consider top directory deleteable.

                ResultCode rc = this.trimDirByAge(directoryToTrim, tooOld, ref totalSize, ref directoryDeleted, ref countTrimByAge);

                if (rc != ResultCode.rcOk)
                {
                    // Basically, need to ask for manual help.
                    return rc;
                }
                // We removed all the stuff that was too old. Do we now have something of allowable size?
                if (totalSize > maxQuantityInKB * 1024)
                {
                    rc = this.trimDirBySize(directoryToTrim, totalSize, maxQuantityInKB * 1024, ref countTrimBySize);
                }
                return rc;
            }

            public ResultCode sendMail(MailMessage mail, ref string sendMsgLog)
            {
                string badArguments = "";
                if (mail == null) badArguments += "mail:null";

                Console.WriteLine("In send mail: A");

                if (!badArguments.Equals(""))
                {
                    Console.WriteLine("In send mail: ArgEx");
                    throw new ArgumentException(badArguments);
                }

                Console.WriteLine("In send mail: B");

                SmtpClient smtpClient = null; 

                try
                {
                    Console.WriteLine("In send mail: C");

                    mail.From = new MailAddress(@"mailbot.4u@gmail.com", "mailbot.4u"); 

                    smtpClient = new SmtpClient();
                    smtpClient.Port = 587;  // tls port?
                    smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                    smtpClient.Host = "smtp.gmail.com";
                    smtpClient.EnableSsl = true;
                    smtpClient.Credentials = new NetworkCredential(@"mailbot.4u@gmail.com", @"@$n2c2b43DSnt23$_)^@#@jdbnskjdbt@RHNI@");

                    Console.WriteLine("In send mail: D");

                    smtpClient.Send(mail);

                    Console.WriteLine("Called Send on smtp");

                    return ResultCode.rcOk; 
                }
                catch (Exception e)
                {
                    Console.WriteLine("Called Send on smtp");
                    sendMsgLog += "Mail problem: " + e.Message;
                }
                finally
                {
                    Console.WriteLine("In send mail: F");
                    if (smtpClient != null) 
                        smtpClient.Dispose();                      
                }
                return ResultCode.rcError; 
            }

            /**
             * Trims a directory based on age. Anything older than the threshold is deleted.
             * As this function travels from root to leaves, it also counts the remaining amount of bytes
             * in the directory.
             * 
             * Deletes subdirectories if empty. Does not delete the main directory.
             * 
             * Expects the caller to have checked the arguments thouroughly - catches only the most generic of 
             * exceptions.
             */
            private ResultCode trimDirByAge(DirectoryInfo dir, DateTime threshold, ref long totalRemaining, ref bool directoryDeleted, ref int countTrimmed, int depth = 0)
            {
                ResultCode rc = ResultCode.rcError;

                int fileCount = 0, directoryCount = 0;

                // If we are in depth 10+, something is seriously wrong. Call for manual help.
                if (depth > 9) return ResultCode.rcDirectoryNestingTooDeep;

                try
                {
                    FileInfo[] fileInfos = dir.GetFiles();
                    fileCount = fileInfos.Length;

                    foreach (FileInfo fi in fileInfos)
                    {
                        if (fi.CreationTime < threshold)
                        {
                            fi.Delete();
                            ++countTrimmed;
                            fileCount--;
                        }
                    }

                    DirectoryInfo[] directories = dir.GetDirectories();

                    directoryCount = directories.Length;

                    foreach (DirectoryInfo di in directories)
                    {
                        directoryDeleted = false;
                        rc = this.trimDirByAge(di, threshold, ref totalRemaining, ref directoryDeleted, ref countTrimmed, depth + 1);

                        if (rc != ResultCode.rcOk) return rc;

                        if (directoryDeleted) directoryCount--;
                    }

                    // If we are in a sub directory, if we've deleted all files and further subdirectories, remove this directory too.
                    // Otherwise count the size of the files left.
                    if (depth > 0)
                    {
                        if (fileCount == 0 && directoryCount == 0)
                        {
                            dir.Delete();
                        }
                        else
                        {
                            foreach (FileInfo fi in fileInfos)
                            {
                                totalRemaining += fi.Length;
                            }
                        }
                    }
                }
                catch (Exception)
                {
                    return ResultCode.rcError;
                }
                return ResultCode.rcOk;
            }
            /**
             * Current size must be an accumulation of all bytes of content in a directory. This function does not
             * scan to count. 
             */
            private ResultCode trimDirBySize(DirectoryInfo dir, long currentSize, long maxSize, ref int countTrimmed)
            {
                // Entry values not checked prior? Consider it an error.
                if (currentSize < maxSize) return ResultCode.rcArgumentBelowMinimum;

                DateTime oldestFound = DateTime.Now;

                try
                {
                    FileInfo[] fileInfos = dir.GetFiles();

                    DirectoryInfo[] directoryInfos = dir.GetDirectories();

                    DirectoryInfo diFound = null;
                    foreach (DirectoryInfo di in directoryInfos)
                    {
                        if (di.CreationTime < oldestFound)
                        {
                            oldestFound = di.CreationTime;
                            diFound = di;
                        }
                    }
                    if (diFound != null)
                    {
                        ++countTrimmed;
                        diFound.Delete(true);
                    }
                }
                catch (Exception)
                {
                    return ResultCode.rcError;
                }
                return ResultCode.rcOk;
            }
            private ResultCode deleteAmount(DirectoryInfo dir, ref long maxByteToDelete)
            {
                if (maxByteToDelete <= 0) return ResultCode.rcOk;

                long size;

                try
                {
                    FileInfo[] fileInfos = dir.GetFiles();

                    foreach (FileInfo fi in fileInfos)
                    {
                        size = fi.Length;
                        fi.Delete();
                        maxByteToDelete -= size;

                        if (maxByteToDelete <= 0)
                        {
                            return ResultCode.rcOk;
                        }
                    }
                    // Any subdirs? 
                    DirectoryInfo[] directoryInfos = dir.GetDirectories();

                    foreach (DirectoryInfo di in directoryInfos)
                    {
                        deleteAmount(di, ref maxByteToDelete);
                    }


                }
                catch (Exception)
                {
                    return ResultCode.rcError;
                }
                return ResultCode.rcOk;
            }

            /**
             * Recursively get the sizes of all files in given directory and all subdirectories.
             * @return rcError in case something went wrong, rcOk if the count done was valid.
             * 
             * @depricated This function is depricated in this context. Trim
             */
            private ResultCode getFullDirSize(DirectoryInfo dir, ref long totalSize)
            {
                try
                {
                    FileInfo[] fileInfos = dir.GetFiles();

                    foreach (FileInfo fi in fileInfos)
                    {
                        totalSize += fi.Length;
                    }

                    DirectoryInfo[] directories = dir.GetDirectories();

                    foreach (DirectoryInfo di in directories)
                    {
                        this.getFullDirSize(di, ref totalSize);
                    }
                }
                catch (Exception)
                {
                    return ResultCode.rcError;
                }
                return ResultCode.rcOk;
            }
        }
    }
}
