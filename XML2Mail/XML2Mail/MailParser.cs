﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Mail; 

using System.Xml.Linq; 

namespace XML2Mail
{
    class MailParser
    {
        public ResultCode ParseMail(XDocument input, XDocument mailConfiguration, ref MailMessage mailMessage, ref string parseLog)
        {
            string argumentCheck = "";

            if (input == null) argumentCheck += "input:null (must be a valid input file as an XDocument)";
            if (mailConfiguration == null) argumentCheck += "mainConfiguration:null (must be a valid job mail configuration file as an XDocument)";
            if (parseLog == null) argumentCheck += "parseLog:null";

            if (!String.IsNullOrEmpty(argumentCheck)) throw new ArgumentException("parseMail bad arguments: " + argumentCheck);

            try
            {
                mailMessage = new MailMessage();
            } catch(Exception e)
            {
                parseLog += "Could not create a mail message. Got: " + e.ToString();
                return ResultCode.rcError; 
            }

            foreach (XNode xn in mailConfiguration.DescendantNodes())
            {
                if (!(xn is XElement))
                {
                    continue; // Only interested in Elements, every value we read comes with a name. 
                }

                if(String.IsNullOrEmpty( ((XElement)xn).Value.ToString()))
                {
                    parseLog += "Null or empty value for configuration node: " + ((XElement)xn).Name.ToString();
                    return ResultCode.rcInvalidInput; 
                }

                ResultCode rc;
                string val = "" ; 

                switch (((XElement)xn).Name.ToString())
                {
                    case "To":
                        mailMessage.To.Add( ((XElement)xn).Value.ToString());
                        break; 
                    case "Subject":
                        mailMessage.Subject = ((XElement)xn).Value.ToString(); 
                        break;
                    case "Body":
                        rc = ParseMailBody(input, ((XElement)xn).Value.ToString(), ref val, ref parseLog); 

                        if(rc != ResultCode.rcOk)
                        {
                            Console.WriteLine("Bad: " + rc); 
                        }

                        mailMessage.Body = val ;
                        break; 
                }
            }                     
            return ResultCode.rcOk; 
        }
        public ResultCode ParseMailBody(XDocument input, string mailBodyTemplate, ref string mailBody, ref string executionLog)
        {
            bool done = false, valid = true, found = false ; 
            int tagEndIdx = 0, lineCount = 0, charCount = 0;
            char c;
            List<string> tags = new List<string>();
            ResultCode rc;

            Console.WriteLine('A'); 

            for(int cnt = 0; cnt < mailBodyTemplate.Length && valid; ++cnt)
            {                
                switch (mailBodyTemplate[cnt])
                {
                    case '\n':
                        ++lineCount; charCount = 0; mailBody += '\n'; 
                        continue;
                    case '/':
                        if (cnt + 1 < mailBodyTemplate.Length)
                        {
                            if(mailBodyTemplate[cnt + 1].Equals('*')) // Comment tag
                            {
                                found = false;
                                string skipped = ""; 
                                for(int cntC = cnt; cntC < mailBodyTemplate.Length; ++cntC)
                                {
                                    if (!mailBodyTemplate[cntC].Equals('*'))
                                    {
                                        skipped += mailBodyTemplate[cntC]; 
                                        continue;
                                    }

                                    if(cntC + 1 < mailBodyTemplate.Length)
                                    {
                                        if(mailBodyTemplate[cntC + 1].Equals('/'))
                                        {
                                            found = true;
                                            cnt = cntC + 1; // Skip / too.
                                            break; 
                                        } else
                                        {
                                            skipped += mailBodyTemplate[cntC + 1];
                                            continue; 
                                        }
                                    }

                                }
                                Console.WriteLine("Skipped: " + skipped);
                                Console.WriteLine("After skip remains: " + mailBodyTemplate.Substring(cnt+1)); 

                                if (found) continue;

                                // Reached end without proper closing. Error
                                executionLog += "Found start of comment at line " + lineCount + " pos " + charCount + " without closure. Please close /* with */";
                                return ResultCode.rcArgumentBadFormat; 

                            } else {
                                ++charCount;
                                mailBody += mailBodyTemplate[cnt]; 
                            }
                        } else {
                            ++charCount;
                            mailBody += mailBodyTemplate[cnt];
                        }
                        continue; 
                    case '#':
                        Console.WriteLine('T');
                        if (cnt + 1 < mailBodyTemplate.Length)
                        {
                            // Double # = write # and do not consider the second. 
                            if (mailBodyTemplate[cnt + 1].Equals('#'))
                            {
                                mailBody += '#';
                                ++cnt; // Skip that second '#'
                                continue;
                            }

                            // Let's see if we got a tag
                            if (mailBodyTemplate[cnt + 1].Equals('S'))
                            {
                                // Ok let's scan to the end of the tag. 
                                tagEndIdx = cnt + 1;
                                done = false; 
                                while (!done)
                                {
                                    if (tagEndIdx + 1 < mailBodyTemplate.Length)
                                    {
                                        if (mailBodyTemplate[tagEndIdx + 1].Equals('#'))
                                        {
                                            if (tagEndIdx + 2 < mailBodyTemplate.Length)
                                            {
                                                if (mailBodyTemplate[tagEndIdx + 2].Equals('S'))
                                                {
                                                    Console.WriteLine("Considering this tag: " + mailBodyTemplate.Substring(cnt + 2, tagEndIdx - 2 - cnt)); 

                                                    rc = handleTag(input, mailBodyTemplate.Substring(cnt + 2, tagEndIdx - 2 - cnt), ref mailBody);

                                                    if (rc != ResultCode.rcOk)
                                                    {
                                                        executionLog += "Skipping bad tag (code: " + rc + "): " + mailBodyTemplate.Substring(cnt + 2, tagEndIdx - 2 - cnt); 
                                                    }
                                                    
                                                    cnt = tagEndIdx + 2; // Continue search after parse.
                                                    done = true;
                                                }
                                                else
                                                {
                                                    executionLog += "Illegal # inside a tag (line:" + lineCount + " pos:" + charCount + "). Use #S<parser command>#S where parser command may not contain a #.";
                                                    valid = false;
                                                    done = true;
                                                }
                                            }
                                            else
                                            {
                                                executionLog += "Unclosed tag (line:" + lineCount + " pos: end of template)" ;
                                                valid = false;
                                                done = true;
                                            }
                                        } else // Not a '#' - check next.
                                        {
                                            ++tagEndIdx; 
                                        }
                                    } else
                                    {
                                        executionLog += "Unclosed tag (line:" + lineCount + " pos: end of template)";
                                        valid = false;
                                        done = true;
                                    }
                                }
                            }
                        }
                        continue; 
                    default:
                        ++charCount; 
                        mailBody += mailBodyTemplate[cnt];
                        continue; 
                }
            }

            return ResultCode.rcOk; 
        }
        private ResultCode handleTag(XDocument input, string tagToHandle, ref string toAddTo)
        {
            Console.WriteLine("Eyooo"); 

            // Let's trim spaces.
            tagToHandle = tagToHandle.Trim(' ');
            // Check path parts
            string[] parts = tagToHandle.Split('.');

            IEnumerable<XElement> xes ;

            List<XElement> parents = new List<XElement>() ;
            int cnt = 1; // The first part of a tag is not a path part but a different directive.

            foreach (XElement xe in input.Descendants(parts[cnt]))
            {
                parents.Add(xe); 
            }

            if (parents.Count() == 0)
                return ResultCode.rcFieldNotFound; 

            List<XElement> children;
            ++cnt;

            while (cnt < parts.Count() - 1) // The last part is a different directive.
            { 
                foreach (XElement xe in parents)
                {
                    children = new List<XElement>();

                    xes = xe.Descendants(parts[cnt]);
                    foreach (XElement xc in xes)
                    {
                        children.Add(xc);
                    }

                    parents = children;
                }
                ++cnt; 
            }
            // Did we find any applicables?
            if(parents.Count() == 0)
            {
                return ResultCode.rcFieldNotFound; 
            }

            string name = null ;
            ResultCode rc = ResultCode.rcError;
            bool found = false; 

            // Check that last directive.
            switch(parts.Last<string>().First<char>())
            {
                case 'A':
                    rc = getPostDirectiveAttributeName(parts.Last<string>(), ref name);
                    if (rc != ResultCode.rcOk) return rc; 

                    foreach (XAttribute xa in parents.First<XElement>().Attributes())
                    {
                        Console.WriteLine("Check attribute: " + xa.Name + ":" + xa.Value); 
                        if(xa.Name.ToString().Equals(name))
                        {
                            toAddTo += xa.Value;
                            found = true; 
                        }
                    }
                    if (!found) return ResultCode.rcFieldNotFound; 
                    return ResultCode.rcOk;
                case 'V':
                    toAddTo += parents.First<XElement>().Value;
                    return ResultCode.rcOk; 
                default:
                    return ResultCode.rcArgumentBadFormat; 
            } 
        }      

        private ResultCode getPostDirectiveAttributeName(string directive, ref string attributeName)
        {
            if (directive.Length < 3) return ResultCode.rcArgumentBadFormat;

            if (!directive[0].Equals('A') || !directive[1].Equals('@')) return ResultCode.rcArgumentBadFormat;

            attributeName = directive.Substring(2);
            Console.WriteLine("post directive, attribute name: " + attributeName); 

            return ResultCode.rcOk; 
        }
        
        /*
         * X.Y.Z
         *  .Y.Q
         * X.Q.Z
         * X.Y.Z
         * X.Y.Q
         * 
         * Search for X.Y.Q:
         * Parent list of X
         * Child list add Y's. For each X, add any Y to Child list.
         * Child list becomes parent list. 
         * 
         */

    }
}
