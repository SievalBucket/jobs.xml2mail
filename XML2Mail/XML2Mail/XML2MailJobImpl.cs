﻿using System;
using System.Collections.Generic;

using SDS.Scheduler;
using System.ComponentModel.Composition;
using System.Net.Mail;
using System.Security;
using System.IO;
using System.Xml.Linq;


namespace XML2Mail
{
    public enum ResultCode
    {
        rcOk = 0,
        rcError,                    // Undefined error, either can't determine, didn't think about, or don't care. 
        rcFieldNotFound,
        rcNoInput,
        rcAlreadySet,
        rcArgumentMayNotBeNull,
        rcArgumentMayNotBeEmpty,
        rcArgumentBelowMinimum,
        rcArgumentAboveMaximum,
        rcArgumentBadFormat,
        rcArgumentInvalidCount,
        rcDirectoryNestingTooDeep,
        rcMoreWork,                 // Success, but not done yet.
        rcBadCleanup,
        rcNotAllowed,
        rcSourceUnreachable,
        rcInvalidInput,
        rcBusy,
        rcResourceDoesNotExist
    }
    [Export(typeof(IJob))]
    public class XML2MailJobImpl : IJob
    {
        #region JobMessages
        [JobMessage]
        public const String MSG_DEBUG   = "MSG_DEBUG";
        [JobMessage]
        public const String MSG_INFO    = "MSG_INFO";
        [JobMessage]
        public const String MSG_ERROR   = "MSG_ERROR";
        [JobMessage]
        public const String MSG_FATAL   = "MSG_FATAL";
        [JobMessage]
        public const String MSG_WARN    = "MSG_WARN";
        #endregion

        #region JobParameters
        /// <summary>
        /// This parameter contains the ConnectionString to connect to the database.
        /// </summary>
        [JobParameter(ParameterName = "Connection string")]
        public String ConnectionString { get; set; }

        /** Input directory, scanned for new files. */
        [JobParameter(ParameterName = "Input directory")]
        public String dirInput { get; set; }
        /** 
        * A file is moved to the work directory before it is handled. This is done so there's a guarantee
        * of a file move prior to handling, so handled files are never considered fresh input. There's still some
        * gripes with this, for example, where to you keep a valid file that could not be handled because of
        * some external issue (like the database not being up)? Place it back in the input directory?
        * Basically this file handling calls for persistence of a job state (database), but for now..
        */
        [JobParameter(ParameterName = "Work directory")]
        public String dirWork { get; set; }
        /** 
        * Succesful processed files are placed in the done directory.
        */
        [JobParameter(ParameterName = "Done directory")]
        public String dirDone { get; set; }
        /**
        * Files that caused problems are placed in the faulty input directory.
        */
        [JobParameter(ParameterName = "Fault directory")]
        public String dirFaulty { get; set; }

        [JobParameter(ParameterName = "Config file path")]
        public String fileConfig { get; set; }

        [JobParameter(ParameterName = "Maximum processed size (in KB)")]
        public String maxDoneSizeKB { get; set; }

        [JobParameter(ParameterName = "Maximum age processed (in days)")]
        public String maxDoneAgeDays { get; set; }
        #endregion

        // These should really be fixed and a part of logger context.
        // public static string LOG_LVL_INFO =     "INFO";
        // public static string LOG_LVL_ERROR =    "ERROR";
        // public static string LOG_LVL_WARN =     "WARNING";
        // Debug messages can just be dumped by the programmer elsewhere. No debug level for the job logger here.

        public static int JOB_INPUT_DIR_IDX = 0;
        public static int JOB_WORK_DIR_IDX = 1;
        public static int JOB_DONE_DIR_IDX = 2;
        public static int JOB_FAULT_DIR_IDX = 3;        
        private int MAX_JOB_IDX = 4; // Increase when expanding above indices.

        public void jInfo(string msg) { jobContext.LogMessage(MSG_INFO, msg); }
        public void jWarn(string msg) { jobContext.LogMessage(MSG_WARN, msg); }
        public void jError(string msg) { jobContext.LogMessage(MSG_ERROR, msg); }
        public void jDebug(string msg) { jobContext.LogMessage(MSG_DEBUG, msg); }
        public void jFatal(string msg) { jobContext.LogMessage(MSG_FATAL, msg); }

        public void Execute(IJobContext jobContext)
        {
            try
            {
                this.jobContext = jobContext; 
                ResultCode rc = ResultCode.rcError;             // start with undefined error.
                DirectoryInfo[] jobDirs = new DirectoryInfo[MAX_JOB_IDX];
                FileInfo fileUri = null;
                string executionInfo = "";

                XDocument mailConfig = null;

                rc = this.jobIO.readFile(fileConfig, ref mailConfig, ref executionInfo); 

                if(rc != ResultCode.rcOk)
                {
                    jFatal("Could not read configuration file: " + fileConfig + " (code:" + rc + ")");
                    return; 
                }

                if (VerifyJobPrerequisities(jobContext, ref jobDirs))
                {
                    XDocument toHandle = null;                 // Stack ref. 

                    rc = jobIO.readInput(jobContext,
                            jobDirs[JOB_INPUT_DIR_IDX],
                            jobDirs[JOB_WORK_DIR_IDX],
                            jobDirs[JOB_FAULT_DIR_IDX], ref toHandle, ref fileUri, ref executionInfo);

                    if (rc == ResultCode.rcNoInput)
                    {
                        jobContext.UpdateProgress("No input found, done.");
                        return;
                    }
                    else if (rc == ResultCode.rcInvalidInput)
                    {
                        jInfo("Input file: " + fileUri + " is considered of invalid format and could not be loaded. Moving it to faulty input.");

                        // Let's place this file in the faulty input directory, along with a message.
                        rc = moveFaultyInput(jobContext, "XML exception: " + executionInfo, jobDirs[JOB_FAULT_DIR_IDX], fileUri);

                        // Can't do much with the result code, we're in a void and above already informs user. 
                        return;
                    }

                    MailParser mp = new MailParser();
                    MailMessage mm = null; // parseMail will allocate.

                    jInfo("Calling parseMail"); 

                    rc = mp.ParseMail(toHandle, mailConfig, ref mm, ref executionInfo); 

                    if(rc != ResultCode.rcOk)
                    {
                        jError("Could not parse mail, code: " + rc + " more info: " + executionInfo);
                        return; 
                    }

                    jInfo("Calling sendMail");

                    rc = jobIO.sendMail(mm, ref executionInfo); 

                    if(rc != ResultCode.rcOk)
                    {
                        jError("Could not send mail, code: " + rc + " more info: " + executionInfo);
                        return;
                    }

                    jInfo("Calling moveHandledInput");

                    rc = moveHandledInput(jobContext, jobDirs[JOB_DONE_DIR_IDX], fileUri);

                    if (rc != ResultCode.rcOk)
                    {
                        jobContext.LogMessage(XML2MailJobImpl.MSG_ERROR, "Could not move file: " + fileUri + " to handled directory!");
                    }
                    return; // Can't inform caller.                       
                }
                else
                {
                    jobContext.UpdateProgress("Not able to verify job prerequisities, code: " + rc);
                }
                jobContext.UpdateProgress("Executed.");
            }
            catch (Exception e)
            {
                jobContext.LogMessage(XML2MailJobImpl.MSG_FATAL, "An unexpected error occurred - this is a bug for the programmer. Extra info (stack trace): " + e.Message);
                // Can't inform caller.
            }
            // Can't inform caller.
        }
        /**
        * The job Initialize function, without a job context parameter, so you can not inform the user
        * that the job cannot start. A good thing we can return .. nothing. 
        * ???
        */
        public void Initialize()
        {
            this.jobIO = new XML2Mail.JobIO(); // Suppose this can exist here...
        }
        /**
            * Checks whether all prerequisities to execute the job are met.
            */
        private bool VerifyJobPrerequisities(IJobContext jobContext, ref DirectoryInfo[] jobDirectories)
        {
            // Input directory.
            string currentDirPath = null;
            string currentDirName = null;

            // Attempt to get the information of all job directories. 
            try
            {
                currentDirPath = this.dirInput; currentDirName = "Input";
                jobDirectories[JOB_INPUT_DIR_IDX] = new DirectoryInfo(currentDirPath);
                if (!createIfNotExists(jobContext, currentDirName, jobDirectories[JOB_INPUT_DIR_IDX])) return false;

                currentDirPath = this.dirWork; currentDirName = "Work";
                jobDirectories[JOB_WORK_DIR_IDX] = new DirectoryInfo(currentDirPath);
                if (!createIfNotExists(jobContext, currentDirName, jobDirectories[JOB_INPUT_DIR_IDX])) return false;

                currentDirPath = this.dirDone; currentDirName = "Done";
                jobDirectories[JOB_DONE_DIR_IDX] = new DirectoryInfo(currentDirPath);
                if (!createIfNotExists(jobContext, currentDirName, jobDirectories[JOB_INPUT_DIR_IDX])) return false;

                currentDirPath = this.dirFaulty; currentDirName = "Fault";
                jobDirectories[JOB_FAULT_DIR_IDX] = new DirectoryInfo(currentDirPath);
                if (!createIfNotExists(jobContext, currentDirName, jobDirectories[JOB_INPUT_DIR_IDX])) return false;

                if(!File.Exists(this.fileConfig)) return false;
            }
            catch (ArgumentNullException ane)
            {
                jobContext.LogMessage(MSG_ERROR, "Missing the " + currentDirName + " directory: " + ane.Message);
                return false;
            }
            catch (SecurityException se)
            {
                jobContext.LogMessage(MSG_ERROR, "Not allowed to access the " + currentDirName + " directory: " + se.Message);
                return false;
            }
            catch (ArgumentException ae)
            {
                jobContext.LogMessage(MSG_ERROR, "The " + currentDirName + " directory [" + currentDirPath + "] was of invalid format: " + ae.Message);
                return false;
            }
            catch (PathTooLongException ptle)
            {
                jobContext.LogMessage(MSG_ERROR, "The " + currentDirName + " directory given [" + currentDirPath + "] was too long. " + ptle.Message);
                return false;
            }
            catch (Exception e)
            {
                jobContext.LogMessage(MSG_ERROR, "An unpexpected exception occurred regarding the " + currentDirName + " directory [" + currentDirPath + "]: " + e.Message);
                return false;
            }
            return true;
        }

        public DateTime getMaxAgeAllowedDone(IJobContext jobContext)
        {
            int maxDaysAgo = 0;
            try
            {
                Int32.TryParse(this.maxDoneAgeDays, out maxDaysAgo);
            }
            catch (Exception)
            {
                jobContext.LogMessage(XML2MailJobImpl.MSG_WARN,
                    "Could not turn max age allowed of processed input (" + this.maxDoneAgeDays + ") into a number. Please enter a valid number of days.");
            }

            if (maxDaysAgo == 0)
            {
                jobContext.LogMessage(XML2MailJobImpl.MSG_WARN, "Max age of allowed processed input is set at 0 days ago, choosing minimum of 1 instead.");
                maxDaysAgo = 1;
            }

            if (maxDaysAgo > 0)
                maxDaysAgo = -maxDaysAgo;


            return DateTime.Now.AddDays(maxDaysAgo);
        }

        public int getMaxDoneSizeKB(IJobContext jobContext)
        {
            int maxSize = 0;
            try
            {
                Int32.TryParse(this.maxDoneSizeKB, out maxSize);
            }
            catch (Exception)
            {
                jobContext.LogMessage(XML2MailJobImpl.MSG_WARN,
                    "Could not turn max size allowed of processed input (" + this.maxDoneSizeKB + ") into a number. Please enter a valid number of kilobytes (but bigger than 100).");
            }

            if (maxSize < 100)
            {
                jobContext.LogMessage(XML2MailJobImpl.MSG_WARN,
                    "Maximum size in KB (" + maxSize + ") for processed input is below minimum (100). Choosing minimum instead.");
                return 100;
            }
            return maxSize;
        }

        private ResultCode moveHandledInput(IJobContext jobContext, DirectoryInfo diTargetDir, FileInfo fileToMove)
        {
            ResultCode rc = this.trimAndMessage(jobContext, diTargetDir, getMaxAgeAllowedDone(jobContext), getMaxDoneSizeKB(jobContext));

            rc = jobIO.moveJobFileTo(null, fileToMove, diTargetDir, false);

            if (rc != ResultCode.rcOk)
            {
                jobContext.LogMessage(XML2MailJobImpl.MSG_ERROR,
                    "A problem occurred trying to move a file (" + fileToMove.FullName + ") to the faulty input directory. Please move it manually before trying again.");
                return ResultCode.rcError;
            }
            return ResultCode.rcOk;
        }


        /**
            * Moves an input file to the faulty input directory.
            * Call this in case something goes wrong during job execution relating to the input read (if the problem is not correlated to 
            * its format / contents leave it where it is). 
            * getMaxFaultySizeKB
            */

        private ResultCode moveFaultyInput(IJobContext jobContext, string msg, DirectoryInfo diTargetDir, FileInfo fileToMove)
        {
            if (fileToMove == null)
            {
                jobContext.LogMessage(XML2MailJobImpl.MSG_ERROR, "Problem: call to move a faulty input file, but no file was given. This is likely a bug.");
                return ResultCode.rcArgumentBadFormat;
            }


            // First do a cleanup run. TODO move these to job configuration parameters. Allow a megabyte (number passed is considered KB)
            // Trimming faulty directory has been removed by product owner. 
            // ResultCode rc = this.trimAndMessage(jobContext, diTargetDir, getMaxAgeAllowedFaulty(jobContext), getMaxFaultySizeKB(jobContext));

            ResultCode rc = jobIO.moveJobFileTo(msg, fileToMove, diTargetDir, true);

            if (rc != ResultCode.rcOk)
            {
                jobContext.LogMessage(XML2MailJobImpl.MSG_ERROR,
                    "A problem (code: " + rc + ") occurred trying to move a file (" + fileToMove.FullName + ") to the faulty input directory. Please move it manually before trying again.");
                return ResultCode.rcError;
            }
            return ResultCode.rcOk;
        }

        private ResultCode trimAndMessage(IJobContext jobContext, DirectoryInfo diToTrim, DateTime tooOld, int maxQuantityInKB)
        {
            int countTrimByAge = 0, countTrimBySize = 0;

            // First do a cleanup run. TODO move these to job configuration parameters. Allow a megabyte (number passed is considered KB)
            ResultCode rc = jobIO.trimDir(diToTrim, tooOld, maxQuantityInKB, ref countTrimByAge, ref countTrimBySize);

            if (rc != ResultCode.rcOk)
            {
                jobContext.LogMessage(XML2MailJobImpl.MSG_ERROR,
                    "A problem occurred cleaning up directory " + diToTrim.FullName + ". Please clean it up manually before trying again.");
                return ResultCode.rcBadCleanup;
            }

            if (countTrimByAge + countTrimBySize > 0)
            {
                jobContext.LogMessage(XML2MailJobImpl.MSG_INFO, "Cleaned up " + (countTrimByAge + countTrimBySize) + " file(s) in directory " + diToTrim.FullName);
            }
            return rc;
        }

        /**
            * Checks if a directory exists. If not, attempts to create it. 
            * @return true if the directory exists, false otherwise.
            */
        private bool createIfNotExists(IJobContext jobContext, string jobDirLabel, DirectoryInfo di)
        {
            if (!di.Exists)
            {
                jobContext.LogMessage(XML2MailJobImpl.MSG_WARN, "The " + jobDirLabel + " directory: " + di.FullName + " does not exist. Attempting to create it.");

                try
                {
                    di.Create();
                }
                catch (IOException ioe)
                {
                    jobContext.LogMessage(MSG_ERROR, "The " + jobDirLabel + " directory: " + di.FullName + " could not be created, because: " + ioe.Message + " - cannot run the job.");
                }

                return false;
            }
            return true;
        }

        public string Description { get; } = "XML2Mail, for all your put-some-stuff-from-this-xml-in-a-mail scenarios.";
        private XML2Mail.JobIO jobIO;
        private IJobContext jobContext = null; 
    }   
}
